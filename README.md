# BBD_REPARTIES_2

### Bienvenue sur notre repo du 2ème rendu de base de données réparties. Le compte-rendu est disponible au format PDF [BDD2_compterendu.pdf](https://gitlab.com/arthur.chazal/bbd_repartis_2/-/blob/main/BDD2_compterendu.pdf)
## Sommaire :

- Exercice 1 : Crawling des créatures et index inversé
     - _Python 3.9 - Crawler_
        - Dossier /creatures_crawler/creatures_crawler/spiders/crawling_creatures.py
            → pour générer le JSON : scrapy crawl creatures_crawler -O creatures.json
    - _Scala - Index inversé_
        - Devoir2.scala

- Exercice 2 : [Application de recherche de sort](https://drive.google.com/file/d/1RPxtnOewW8Lm3eTIIuxDX4UKTYHznZOL/view?usp=sharing)
    - _Electron JS_
    - _Utilisation de MongoDB Altas (connexion internet obligatoire)_
