import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config._
import org.apache.spark.sql.{DataFrame, Encoder, Encoders, SaveMode, SparkSession}
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Exercice1 extends App {
  val spark = SparkSession.builder()
    .master("local")
    .appName("MongoSparkConnectorIntro")
    .config("spark.mongodb.input.uri", "mongodb://127.0.0.1/test.creature")
    .config("spark.mongodb.output.uri", "mongodb://127.0.0.1/test.creature")
    .getOrCreate()
  spark.sparkContext.setLogLevel("WARN")
  import spark.implicits._
  val rdd = MongoSpark.load(sparkSession = spark).rdd
  val test=rdd.flatMap(row=>
    {
      val results = new ArrayBuffer[(String, String)]()
      val spells=row.getAs[mutable.WrappedArray[String]]("spells")
      spells.foreach(spell=>results+=Tuple2(spell.trim().capitalize,row.getAs[String]("name").trim()))
      results
    }).sortBy(r=>r)
  val batch_view=test.reduceByKey(_+" | "+_).sortByKey()
  batch_view.foreach(ligne=>println(Console.BLUE+"Spell: "+Console.MAGENTA++ligne._1+Console.BLUE+" Creature: "+Console.MAGENTA+ligne._2))

  val MongoRDDSave = batch_view.map(line=>(line._1,line._2.split(""" \| """))) //AFIN DE STOCKER LA BATCH VIEW DANS MONGODB AVEC (STRING,ARRAY[STRING]) pour les recherches avancées
  MongoRDDSave.foreach(ligne=>println(ligne))
  MongoRDDSave.toDF().write.format("mongo")
    .option("url", "localhost:27017")
    .option("database", "test")
    .option("collection", "creatures_by_spell")
    .mode(SaveMode.Overwrite)
    .save()
}

