# coding: utf-8
import scrapy
from pprint import pprint


class Spider_Creature_Scrawler(scrapy.Spider):
    # Nom Spider
    name = "creatures_crawler"
    # URL_a_crawl
    url = "https://aonprd.com/Monsters.aspx?Letter=All"
    url2 = "https://www.d20pfsrd.com/bestiary/monster-listings"

    def start_requests(self):
        yield scrapy.Request(url=self.url2, callback=self.parse_all_creatures)

    def parse_all_creatures(self, response):
        liste_creature_type = response.xpath(
            '//ul[@class="ogn-childpages"]//li//a[not(contains(@href,"templates"))]'
        )
        for creature in liste_creature_type:
            yield scrapy.Request(
                creature.xpath('@href').get(),
                callback=self.parse_creature,
                meta={'item': {
                    'name': creature.xpath('.//text()').get(),
                    'link': creature.xpath('@href').get()
                }})

    def parse_creature(self, response):
        sous_classe = response.xpath('//li[@class="page new parent"]')
        if (len(sous_classe) > 0):
            item = []
            for sous_creature in sous_classe:
                item.append(
                    scrapy.Request(
                        sous_creature.xpath('.//a/@href').get(),
                        callback=self.parse_sous_creature,
                        meta={
                            'item': {
                                'name':
                                sous_creature.xpath('.//a//text()').get(),
                                'link': sous_creature.xpath('.//a/@href').get()
                            }
                        }))
        else:
            item = response.meta['item']
            item['spells'] = response.xpath(
                '//a[@class="spell"]//text()').getall()
        return item

    def parse_sous_creature(self, response):
        item = response.meta['item']
        item['spells'] = response.xpath('//a[@class="spell"]//text()').getall()
        return item


pass
