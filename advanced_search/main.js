const { app, BrowserWindow } = require('electron')
const shell = require('electron').shell
const path = require('path')
try {
  require('electron-reloader')(module)
} catch (_) { }
// modifier votre fonction createWindow()
function createWindow() {
  const win = new BrowserWindow({ 
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  })
  win.maximize()
  win.loadFile('./public/html/main.html')
  win.webContents.on('new-window', function (e, url) {
    // make sure local urls stay in electron perimeter
    if ('file://' === url.substr(0, 'file://'.length)) {
      return;
    }

    // and open every other protocols on the browser
    e.preventDefault();
    shell.openExternal(url);
  });
}
app.commandLine.appendSwitch('enable-features', "SharedArrayBuffer")
app.whenReady().then(() => {
  createWindow()
})
