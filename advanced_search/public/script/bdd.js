const { MongoClient } = require("mongodb");
const uri =
  "mongodb+srv://ouioui_user:ouioui@cluster0.6lpxr.mongodb.net/BDD_reparti?retryWrites=true&w=majority";

var results_html = [];
var results_creature = [];

function loadSpells(numImages = 20) {
  console.log("LoadSpell");
  let i = 0;
  var html_contenair = document.getElementById("results");
  let nb_elem = html_contenair.childNodes.length;
  console.log(
    i +
      "if nb_elem:" +
      nb_elem +
      " / results_html.length:" +
      results_html.length
  );
  while (i < numImages && nb_elem + i < results_html.length) {
    console.log("ici");
    html_contenair.innerHTML += results_html[i + nb_elem];
    i++;
  }
}

document
  .getElementById("bdd_search")
  .addEventListener("submit", function checkForm(event) {
    //Prevents default action that would normally happen onsubmit
    event.preventDefault();
    //Define the form element
    let creatures = [];
    const client = new MongoClient(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    var request_name = requested_name.value;
    var SpellResistance = "";
    var spell_level_min = "0";
    var spell_level_max = "9";
    if (level_min.value != "") spell_level_min = level_min.value;
    if (level_max.value != "") spell_level_max = level_max.value;
    var FullText_request = FullText.value;
    results_html = [];
    var SpellClass = [];
    var Compo = [];
    //On récupère les valeurs des checkboxs
    let RadioButt_SpellRes = document.getElementsByName("SpellResistance");
    RadioButt_SpellRes.forEach((RadioButt) => {
      if (RadioButt.checked) {
        SpellResistance = RadioButt.value === "true";
      }
    });
    let Class_CheckBox = document.getElementsByName("SpellClass");
    Class_CheckBox.forEach((checkbox) => {
      if (checkbox.checked) {
        SpellClass.push(checkbox.value);
      }
    });
    let Compo_CheckBox = document.getElementsByName("CompSpell");
    Compo_CheckBox.forEach((checkbox) => {
      if (checkbox.checked) {
        Compo.push(new RegExp(checkbox.value));
      }
    });
    //On reset les HTML
    document.getElementById("results").innerHTML = "";
    document.getElementById("wait").innerHTML =
      '<div class="spinner-grow text-primary" style="width: 3rem; height: 3rem;" role="status"><span class="visually-hidden">Loading...</span></div>';
    client.connect((err) => {
      if (err) throw err;
      const collection = client.db("BDD_reparti").collection("spells");
      var mysort = { name: 1 };
      //On rentre le nom du form
      var query = { name: { $regex: request_name.toLowerCase() } };
      //On rentre la valeur du boolean SpellResistance du form si un des boutons est coché
      if (typeof SpellResistance == "boolean")
        query["spell_resistance"] = SpellResistance;

      //On rentre dans un tableau les classes formatées pour la query
      var query_tab = [];
      SpellClass.forEach((classe) => {
        query_tab.push(
          new RegExp(
            "^" +
              classe.toLowerCase() +
              ".*[" +
              spell_level_min +
              "-" +
              spell_level_max +
              "]"
          )
        );
      });
      //Si radiobutton AND alors on fait la query correspondante
      if (
        document.querySelector('input[name="OptionClasse"]:checked').value ==
        "AND"
      ) {
        if (query_tab.length > 0) query["level"] = { $all: query_tab };
      } else {
        if (query_tab.length > 0) query["level"] = { $in: query_tab };
      }
      if (query["level"] == null)
        query["level"] = {
          $in: [
            new RegExp(".*[" + spell_level_min + "-" + spell_level_max + "]"),
          ],
        };
      //Différente query en fonction du radiobutton
      switch (
        document.querySelector('input[name="OptionComp"]:checked').value
      ) {
        case "AND":
          if (Compo.length > 0) query["components"] = { $all: Compo };
          break;
        case "OR":
          if (Compo.length > 0) query["components"] = { $in: Compo };
          break;
        case "NOT":
          if (Compo.length > 0) query["components"] = { $not: { $in: Compo } };
          break;
        case "":
        default:
          break;
      }
      //RECHERCHE FullText
      if (FullText_request != "") {
        query["$text"] = { $search: FullText_request };
      }
      //On envoie la query à MONGODB
      console.log(query);
      collection
        .find(query)
        .sort(mysort)
        .toArray(function (err, result) {
          if (err) throw err;
          document.getElementById("nb_result").innerHTML = result.length;
          document.getElementById("wait").innerHTML = "";
          //Pour chaque résultat de la query : On affiche les spells avec leur image et un lien vers aonprd
          result.forEach((spell) => {
            //On formate les résultats pour les afficher correctement avec leur image
            var spell_name = spell.name;
            var spell_name_card = spell.name
              .replace(/\(.[^(]*\)/g, "")
              .replace(/'s/gm, "_s")
              .trim();
            if (spell.creatures != null)
              creatures = creatures.concat(spell.creatures);
            error_code = 'this.outerHTML="<img"';
            results_html.push(
              '<div class="item">' +
                '<h4><a target="_blank" class="link-dark" href="https://aonprd.com/SpellDisplay.aspx?ItemName=' +
                spell_name +
                '">' +
                '<img src="../Spellcards/' +
                spell_name_card +
                '.jpg" alt="' +
                spell_name +
                '" width="70%" onerror="this.src=\'../Spellcards/back.jpg\';this.outerHTML+=\'<p>' +
                spell_name_card +
                "</p>'\"/>" +
                "</a></h4></div>"
            );
          });
          //On charge 20 images par 20 images pour l'utilisation de la RAM
          results_creature = creatures.reduce(
            (a, c) => a.set(c, (a.get(c) || 0) + 1),
            new Map()
          );
          document.getElementById("nb_result_creature").innerHTML =
            results_creature.size;
          loadSpells();
        });
    });
    //RESET du form
    if (SpellResistance)
      document.querySelector(
        'input[name="SpellResistance"]:checked'
      ).checked = false;
    client.close();
  });

var exampleModal = document.getElementById("exampleModal");
exampleModal.addEventListener("show.bs.modal", function (event) {
  const client2 = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  var modalBodyInput = exampleModal.querySelector(".modal-body");
  modalBodyInput.innerHTML = '<ul class="list-group"></ul>';
  results_creature = new Map(
    [...results_creature].sort((a, b) => {
      if (a[1] < b[1]) {
        return 1;
      } else if (a[1] > b[1]) {
        return -1;
      } else {
        return a[0] < b[0] ? -1 : a[0] > b[0] ? 1 : 0;
      }
    })
  );

  client2.connect((err) => {
    if (err) throw err;
    const collection2 = client2.db("BDD_reparti").collection("creatures");
    results_creature.forEach((occurence, creature) => {
      let a = document.createElement("a");
      var query = {
        name: new RegExp(creature.replace("(", "\\x28").replace(")", "\\x29")),
      };
      collection2.find(query).toArray(function (err, result) {
        if (err) throw err;
        console.log(result);
        if (result.length > 0) a.href = result[0].link;
      });
      a.title = creature;
      a.textContent = creature;
      a.target = "_blank";
      a.className = "link-dark";
      let span = document.createElement("span");
      span.className = "badge bg-primary rounded-pill";
      span.textContent = occurence;
      let li = document.createElement("li");
      li.className =
        "list-group-item d-flex justify-content-between align-items-center";
      li.append(a);
      li.append(span);
      modalBodyInput.appendChild(li);
    });
  });
  client2.close();
});

// listen for scroll event and load more images if we reach the bottom of window
window.addEventListener("scroll", () => {
  //visible part of screen
  if (
    window.scrollY + window.innerHeight >=
    document.documentElement.scrollHeight - 300
  ) {
    loadSpells();
    console.log("scrolled");
  }
});
